@extends('viewproductslayout')
@section('content')
<!-- Animated -->
            {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}

            <div class="animated fadeIn">
                
                <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header text-center text-uppercase">
                                        <strong class="card-title ">Terms and Conditions</strong>
                                    </div>
                                    <div class="card-body" style="border:2px solid orange; padding:20px; font-size:12px">
                                            <h4 style="text-decoration:underline" class="text-center">4Pay Kenya’s General Conditions</h4>
                                            <ol>
                                                <li>GENERAL PROVISIONS</li>
                                            
                                            <ol>
                                                <li>These General Conditions define the main principles of communication between Us and You.</li>
                                                <li>The General Conditions constitute an inseparable part of all Customer Relations. In addition to the General Conditions, the relationships between Us and You are governed by legislation, the Customer Contract, the Customer Agreement Conditions, Service Agreements, Service Conditions, Price List and principles of good faith and reasonableness.</li>
                                                <li>In the event of controversies between the General Conditions and the Service Conditions, the Service Conditions apply. In the event of controversies between the General Conditions apply. In the event of controversies between the General Conditions and the Service Conditions or between the Service Conditions and the conditions of the Service Agreement, the conditions of the Service Agreement apply.</li>
                                                <li>The General Conditions, the Customer Agreement Conditions, the Service Conditions, the Price List and the Principles of Processing Customer Data can be accessed on the Website.</li>
                                                <li>The Kenyan laws applies to the relationships between Us and You.</li>
                                                <li>We and You communicate in English or Swahili, unless We have agreed on another language of communication with You. In the event of disagreements, we rely on the Kenyan Conditions.</li>
                                            </ol>
                                            <hr>
                                            <li>ESTABLISHMENT AND AMENDMENT OF CONDITIONS</li>
                                            <ol>
                                                <li>We establish the General Conditions, the Customer Agreement Conditions, the Service Conditions, the Price List and the Principles of Processing Customer Data.</li>
                                                <li>The conditions of the Customer Agreement and Service Agreement are established by agreement between You and Us.</li>
                                                <li>We have the right to unilaterally amend the General Conditions, the Customer Agreement Conditions, the Service Conditions, the Price List and the Principles of Processing Customer Data. We ensure that the respective amendment is reasonable regarding You. We will inform You about the amendment via the Website or another channel chosen by Us at least thirty (30) days in advance, unless otherwise provided by legislation or the Conditions.</li>
                                                <li>We have the right to unilaterally and without advance notification make the Conditions more favorable for You (i.e. grant You additional rights or limit Your duties) or add new Services to the Price List.</li>
                                                <li>In justified cases, We have the right to unilaterally amend the General Conditions, the Customer Agreement Conditions, the Service Conditions, the Principles of Processing Customer Data and the Price List without advance notification. We will inform You about the amendments immediately via the Website or another channel chosen by Us.</li>
                                                <li>If You do not consent to the amendments made by Us, You will have the right to terminate the Customer Agreement and/or the Service Contract in accordance with the procedure set out in the Customer Agreement and/or the Service Agreement within the time limit specified in section 3.2 of the General Conditions. Upon termination of the Customer Agreement and/or the Service Agreement, You agree to perform towards Us all the obligations arising from the Service Agreement. If You do not terminate the Customer Agreement and/or the Service Agreement within the aforementioned term, it will be deemed that You have accepted all the amendments.</li>
                                            </ol>
                                            <hr>
                                            <li>ESTABLISHMENT OF CUSTOMER RELATIONSHIP, CONCLUSION OF CUSTOMER AGREEMENT</li>
                                                <ol>
                                                    <li>A Customer Relationship between You and Us is usually established when we conclude the Customer Agreement with You. By way of exception, we deem the Customer Relationship as established also when You use the Service on the basis of the Service Agreement without concluding the Customer Agreement.</li>
                                                    <li>To conclude a Customer Agreement, submit a respective Request to Us. We will identify You and check the information submitted by You. We will inform You about the conclusion of the Customer Agreement via Our Branch, Online Portal or Our Telecommunications Details.</li>
                                                    <li>We have the right to decide with whom to conclude a Customer Agreement and establish a Customer Relationship. We thoroughly and fully consider all the circumstances that may lead to Our refusal to conclude a Customer Agreement and/or to establish a Customer Relationship. Among other things, the circumstances listed in section 8.3 of the General Conditions lead to the refusal to conclude the Customer Agreement.</li>
                                                    <li>Section 16 of the General Conditions applies to the entry into force, validity and termination of a Customer Relationship.</li>
                                                </ol>
                                                <hr>
                                            <li>IDENTIFYING YOU</li>
                                            <ol>
                                                <li>Upon establishment of a Customer Relationship and provision of the Service, we identify You and ask for additional information. Submit to Us the documents and information requested by Us and make certain that the information given by You is correct and complete. Please inform Us immediately in the event of changes in the information submitted by You.</li>
                                                <li>We identify a natural person based on the documents determined by Us, which comply with the requirements of the legislation in force (e.g. passport, identity document, etc.).</li>
                                                <li>We have the right to identify You in the manner and in accordance with the procedure accepted by Us.</li>
                                            </ol>
                                            <hr>
                                            <li>REPRESENTING YOU</li>
                                            <ol>
                                                <li>Only A Customer who is a natural person can make online transactions with Us.</li>
                                            </ol>
                                            <hr>
                                            <li>REQUIREMENTS FOR DOCUMENTS SUBMITTED BY YOU</li>
                                            <ol>
                                                <li>We do not require to submission of documents to support requisite information in order to initiate a transaction(s) with You. We however reserve the right to request for original documents or copies certified in a manner accepted by Us.</li>
                                                <li>Documents issued in a foreign country must be original documents or documents certified by a notary or in an equal manner. At Our request, the document must be legalised or certified.</li>
                                                <li>We have the right to keep a document submitted by You (except for an identity document) and make a copy thereof.</li>
                                                <li>All documents presented to Us must be in the recognized official languages in Kenya: English or Swahili.</li>
                                                <li>We have the right to assume that each document submitted by You is genuine, valid and correct. If We have doubts about the genuineness or validity or correctness of a document, We will have the right not to make the transaction requested by You and/or not to execute the Order given by You and We have the right to demand that You submit additional information and/or documents.</li>
                                                <li>You bear all the costs relating to the submission of proper documents to Us, incl. to bringing documents into compliance with the requirements.</li>
                                            </ol>
                                            <hr>
                                            <li>CONCLUSION OF SERVICE AGREEMENT</li>
                                            <ol>
                                                <li>Your relationships with Us in relation to the Service are regulated by the Service Agreement.</li>
                                                <li>We will conclude a Service Agreement with You if You and the agreement conditions requested by You comply with the law and the General Conditions. We have the right to decide with whom to conclude a Service Agreement. We thoroughly and fully consider all the circumstances that may lead to Our refusal to conclude the Service Agreement.</li>
                                                <li>We have the right to refuse to conclude a Service Agreement, above all, if:</li>
                                                <li>the Customer or a Related Person has wilfully or due to gross negligence submitted to Us or to a legal person belonging to Us incorrect or insufficient data or refuses to submit data;</li>
                                                <li>the Customer or a Related Person has not submitted to Us or to a legal person belonging to Us enough requested data or documents for being identified or for certifying the legal origin of the funds or there is a suspicion of money laundering or involvement in organised crime regarding the Customer or the Related Person;</li>
                                                <li>the Customer or a Related Person has an overdue sum towards Us or a legal person belonging to the same consolidation group as Us;</li>
                                                <li>an act or omission of the Customer or a Related Person has caused damage to Us or a legal person belonging to the same consolidation group as Us or a real threat of damage (incl. reputation damage);</li>
                                                <li>a document submitted to Us by the Customer appears to be forged or otherwise does not meet Our requirements;</li>
                                                <li>the Customer does not act responsibly or with the diligence normally expected from persons acting in the field of activity;</li>
                                                <li>the Customer does not meet the requirements provided for in the additional recommended measures applied to credit institutions in relationships with foreign legal persons with the aim of improving activities aimed at preventing money laundering;</li>
                                                <li>at the moment of submitting the Request, the Customer is related to or has been related to the traditional sources of income of organised crime, e.g. contraband, narcotic substances, human trafficking, illegal arms trade;</li>
                                                <li>at the moment of or before submitting the Request, the Customer and/or its beneficial owner is or has been a politically exposed person in a state with a high level of corruption;</li>    
                                                <li>at Our estimate, the Customer is operating in a field of activity or country with a high level of money laundering;</li>
                                                
                                                <li>the Customer acts without the required registration or authorisation;</li>
                                                <li>on another ground arising from law, especially if the conclusion of the Service Agreement is impeded by some lawful impediment such as limited active legal capacity or the controversiality or absence of rights of representation.</li>
                                            </ol>
                                            <hr>
                                            <li>
                                                INFORMATION SUBJECT TO FINANCIAL SECRECY, PROCESSING YOUR PERSONAL DATA
                                            </li>
                                            <ol>
                                                <li>We maintain the confidentiality of any information subject to financial secrecy in accordance with the requirements of legislation.</li>
                                                <li>We process the data concerning You in accordance with the Principles of Processing Customer Data.</li>
                                            </ol>
                                            <hr>
                                            <li>SENDING AND RECEIVING NOTICES</li>
                                            <ol>
                                                <h6><strong> Our Notices</strong></h6>
                                                <li>We give Notices to You via telecommunications (incl. e-mail, telephone) or the Website. We deem the details of Your telecommunications that You have sent Us to be correct.</li>
                                                <li>We consider personal Notices sent by Us as received by You and Our notification obligation as fulfilled if Our Notice has been sent to the postal address or the address of the means of communication last indicated by You. We consider a Notice published on the Website as received on the date of publication thereof. We consider a Notice given via the address of a means of communication as received by You on the date of sending the Notice. By way of exception, we consider a Notice sent by regular mail as received if five (5) calendar days have passed from posting it.</li>
                                                <li>Communication between Us and You, Our explanations and the news and information letters sent by Us cannot be considered as investment advice.</li>
                                                <li>Please immediately verify the accuracy and correctness of the information received from Us. Upon detecting inaccuracies, inform Us thereof immediately.</li>
                                                </ol>
                                                <ol>
                                                <h6><strong>Your Notices</strong></h6>
                                                <li>You have the right to submit Notices to Us electronically, on paper or in another agreed manner to Our Telecommunications Details.</li>
                                                <li>You have the duty to inform Us immediately at least in a form reproducible in writing of all the circumstances that have a meaning in communication between Us and You, incl. about the following: if Your data (incl. the name, the address of the seat or place of residence, the telecommunications details) or other information (incl. data on the beneficial owner, etc.) changes.</li>
                                                <li>If You have not performed the notification duty specified in the previous section, We have the right to presume the correctness of the information at Our disposal and We are not liable for damage caused to You and/or Third Parties as a result thereof, unless damage is caused by Us willfully or due to gross negligence.</li>
                                            </ol>
                                            <hr>
                                            <li>YOUR REQUESTS TO US </li>
                                            <ol>
                                                <li>You have the right to submit Requests to Us for the conclusion, amendment or termination of the Customer Agreement and/or Service Agreement. You can submit Requests to Us in the manner and in accordance with the procedure set out in the General Conditions, the Customer Agreement Conditions, the Service Conditions, the Customer Agreement and/or the Service Agreement.</li>
                                                <li>Every one of Your Requests is subject to the provisions of sections 12.2 and 12.3 of the General Conditions.</li>
                                            </ol>
                                            <hr>
                                            <li>YOUR ORDERS TO US</li>
                                            <ol>
                                                <li>You have the right to submit to Us Orders that, as a rule, must be performed by Us, unless otherwise provided by the Conditions. You can submit Orders in accordance with the Service Agreement in the manner and in accordance with the procedure established in the Service Agreement and/or the Customer Agreement.</li>
                                                <li>Your Orders must be properly and duly drawn up, unambiguous, executable and clearly indicate your will. We have the right to assume that the content of an Order given to Us by You meets Your true will. Please be advised that We are not liable for any inaccuracies, mistakes or transmission errors contained in Your Order. </li>
                                                <li>We have the right to record all Orders given by You via a means of communication as well as all other steps, incl. visits of the Website, and use the respective recordings, where necessary, to prove the Orders given by You or to prove other steps.</li>
                                                <li>We execute Your Orders based on Kenyan laws, other legislation and the Conditions. Before executing an Order, We have the right to demand that You, prove the legal origin of the funds used for making a transaction. We have the right not to execute Your Order if You fail to prove the legal origin of the funds used for making the transaction or We otherwise have a suspicion of money laundering regarding the transaction.</li>
                                                <li>We may deviate from Your Order if We can, based on the circumstances, assume with reason that You would approve Our steps.</li>
                                                <li>We have the right to refuse to execute an Order and/or provide a Service if We have a reasonable doubt that the person who wants to use the Service does not have the respective right.</li>
                                                <li>We have the right not to execute Your Order that does not comply with the requirements provided for in section 12.1.</li>
                                                <li>You are required to establish all the prerequisites and conditions dependent on You, which are necessary for the performance of Your Order. If You have not acted in such a manner, We will have the right not to execute Your Order and thereby We are not liable for damage caused to You and/or Third Parties due to the non-execution of the Order.</li>
                                                <li>You have the right to demand that We only perform Orders set out in Our Price List or the execution of which we have agreed on with You separately.</li>
                                                <li>You have the right to cancel Your Order if We have not executed it yet or if We have not assumed obligations towards Third Parties for the purpose of executing it. The cancellation of an Order is subject to the fee established in the Price List.</li>
                                            </ol>
                                            <hr>
                                            <li>PAYMENT OBLIGATIONS, REQUIREMENTS</li>
                                            <ol>
                                                <li>The list and prices of the Services provided by Us are set out in the Price List. You have the obligation to pay for the provided Service or perform a payment obligation that You assumed otherwise under the Price List and the Service Agreement.</li>
                                                <li>In addition to the contents of the Price List or separately concluded agreements, You bear the costs of the steps taken by Us in Your interests (e.g. postal costs, notary fees, unforeseeable additional expenses, custody expenses, etc.).</li>
                                                <li>For the Services specified in the Price List You have to pay Us the fee agreed between You and Us or, upon absence of an agreement, based on Our actual costs.</li>
                                                <li>Our Price List is available on the Website.</li>
                                                <li>We withhold service fees, interests and other sums and payables payable by You from Your Customer Account, unless otherwise agreed in the Service Agreement. Please make certain that You have enough Funds on Your Customer Account for the payment of Your payables, unless Your sum payable is debited from the Customer Account in accordance with the Service Agreement.</li>
                                                <li>We issue invoices by way of notices that shall be sent to you via SMS or email or both.</li>
                                                <li>Unless agreed otherwise or unless otherwise provided by legislation, We have the right to set off mutual claims between You and Us. We will inform You about a set-off in accordance with the Service Agreement concluded with You or in accordance with a legal instrument.</li>
                                                <li>We have the right to choose the order of withholding service fees and other sums and payables payable to Us in accordance with the requirements of legislation. We have the right to withhold from Your Customer Account, firstly, the sums payable to Us, which have fallen due also when, after the sums fell due and before their actual withholding by Us, Your or Third Parties have given Orders with other contents, unless otherwise provided by legislation.</li>
                                                <li>We calculate administration, service fees and late fee charges or penalties established in the Service Agreement. We have the right to unilaterally change the rate and the procedure for calculation of these fees. If the administration, service fees and late fee charges has been agreed in the Service Agreement, we will amend the late fee charges based on the provisions regulating the amendment of the Service Agreement.</li>
                                                <li>We have the right to assign claims against You to a Third Party, unless otherwise provided by a legal instrument.</li>
                                                <li>All Our settlements are made in Kenya Shillings.</li>

                                            </ol>
                                            <hr>
                                            <li>RESTRICTION OF USE OF CUSTOMER ACCOUNT</li>
                                                <h6><strong>Blocking of Customer Account</strong></h6>
                                                <ol>
                                                    <li>The blocking of the Customer Account means the partial or full suspension of transactions with the funds available on the Customer Account.</li>
                                                    <li>Usually, we block Your Customer Account on the basis of Your respective request. We have the right to demand that You confirm a request for the blocking of a Customer Account in writing. If You do not submit a respective confirmation, We will have the right to refuse to block the Customer Account or to unblock the Customer Account. We are not responsible for the damage caused to You as a result thereof.</li>
                                                    <li>We have the right to block the use of the Customer Account in the following events:</li>
                                                    <ol style="list-style:lower-roman">
                                                            <li>You do not submit to Us the documents requested by Us or You submit controversial information to Us;</li>
                                                            <li>You submit to Us documents the correctness of which We have a reason to call into doubt;</li>
                                                            <li>We suspect money laundering or another criminal offence;</li>
                                                            <li>We have reason to suspect that the funds on the Customer Account have been obtained as a result of a criminal offence;</li>
                                                            <li>the blockage is, according to Our estimate, necessary for preventing damage to Us or a Third Party;</li>
                                                            <li>the Customer Account has been fully or partially frozen;</li>
                                                            <li>evidence of the death of the Account Holder has been submitted to Us or we have a good reason to believe that the Account Holder has died;</li>
                                                            <li>the telecommunications details submitted by You or other data concerning You or Your activities turn out to be incorrect and We fail to contact You or receive correct data from You.</li>
                                                    </ol>
                                                    <li>We will unblock the Customer Account not later than within three (3) business days after the circumstances serving as the basis for the blockage have, to Our knowledge, lapsed. Please inform Us of such circumstances.</li>
                                                    <li>We are not liable for damage arising from blocking the Customer Account.</li>
                                                </ol>
                                                <h6><strong>Freezing of Customer Account</strong></h6>
                                                <ol>
                                                    <li>We will freeze Your Customer Account at the request of a Third Party on the conditions and in accordance with the procedure set out in legislation.</li>
                                                    <li>We will unfreeze your Customer Account on the basis of a decision of the Third Party who made the freezing demand or on the basis of a final judgment or in other events provided by legislation not later than within three (3) business days.</li>
                                                    <li></li>
                                                </ol>
                                                <h6><strong>Restrictions of use of information system</strong></h6>
                                                <ol>
                                                    <li>We have the right to carry out regular and special maintenance and development work of Our information system during which the use of the Online Portal is partially or fully restricted. If possible, we carry out maintenance and development work at night.</li>
                                                    <li>Please be advised that the use of the Online Portal may be disturbed from time to time. Our goal is to restore the functioning of the Online Portal as soon as possible. We are not liable for any damage that the improper functioning of the Online Portal may cause You.</li>
                                                </ol>
                                                <hr>
                                                <li>TRANSACTIONS MADE BY MISTAKE</li>
                                                <ol>
                                                    <li>If We have without grounds, transferred funds to the Customer Account, please inform Us thereof immediately. You do not have the right to dispose of the funds transferred to the Customer Account by mistake.</li>
                                                    <li>We have the right to debit the sum transferred to Your Customer Account by mistake without asking for Your consent.</li>
                                                    <li>If We have made a mistake relating to the amount upon executing Your Order, We will have the right to debit Your Customer Account with a corrective transfer and make a new transfer to the Customer Account in accordance with the data given in Your Order.</li>
                                            
                                                </ol>
                                                <hr>
                                                <li>ENTRY INTO FORCE, TERM AND TERMINATION OF SERVICE AGREEMENT</li>
                                                <ol>
                                                    <li>The Service Agreement will enter into force as of the date specified in the Service Agreement.</li>
                                                    <li>The Service Agreement remains in force until the date of expiry or until the unilateral termination of the Service Agreement or until the termination of the Service Agreement by agreement of the Parties.</li>
                                                    <li>We have the right to unilaterally terminate the Service Agreement without advance notification if You have wrongfully and to a considerable extent breached an obligation arising from the Service Agreement, i.e. above all, if:</li>
                                                    <li>You have has submitted the wrong or defective data or documents to Us or You refuse to submit the documents and/or data requested by Us;</li>
                                                    <li>You have not or submitted to Us enough data or documents to verify Your identity or confirm the legal origin of the money or there is a suspicion of money laundering regarding the Customer for another good reason;</li>
                                                    <li>You have an overdue payment towards Us;</li>
                                                    <li>an act or omission of You or a Related Person has caused Us damage or a real threat of damage;</li>
                                                    <li>You have not submitted to Us true information about Your economic situation or You have not informed Us about the deterioration of Your economic situation or other circumstances that may prevent You from properly performing Your obligations;</li>
                                                    <li>on another ground provided by legislation, incl. if the continuance of the Service Agreement is impeded by a lawful impediment such as limited active legal capacity, if the termination of the Service Agreement is demanded by a Kenyan authority.</li>
                                                </ol>
                                                <hr>
                                                <li>LIABILITY</li>
                                                <ol>
                                                    <li>The Parties will duly, in good faith, reasonably and with due diligence as well as taking into account customs and practice perform their duties and obligations arising from the Customer Relationship.</li>
                                                    <li>A Party is liable for damage caused to the other Party by failure to perform or improper performance of their duties or obligations wilfully or due to gross negligence. 4Pay Kenya is not liable for indirect damage.</li>
                                                    <li>A Party is not liable for damage if the breach is excusable, i.e. above all, if the breach was caused by force majeure. Force majeure means, among other things, the disturbance of Our activities by Third Parties (a bomb threat, a cyber-attack, etc.), by an event that is beyond Our control (a strike, a moratorium, power outage, malfunction of the telecommunications line, outages of servers and other information technology services, etc.) or by a public authority.</li>
                                                    <li>We are not liable for damage caused by the following:</li>
                                                    <ol style="list-style:lower-roman">
                                                        <li>Third Parties, incl. via services provided via Us or via communicated information;</li>
                                                        <li>malfunctions in the operation of information systems (incl. the Online Portal);</li>
                                                        <li>Your improper performance of the obligation to notify Us;</li>
                                                        <li>Our unawareness of the absence of the passive legal capacity of a legal person or the limitation of the active legal capacity or absence of the decision-making capacity of a natural person.</li>

                                                    </ol>
                                                    
                                                </ol>
                                                <hr>
                                                <li>COMPLAINTS, RESOLUTION OF DISPUTES</li>
                                                <ol>
                                                    <li>Our goal is to provide You with a high-quality Service. If You are dissatisfied with Our Service or how You are treated, please inform Us thereof in accordance with the procedure for resolution of complaints available on the Website. Submit Your complaint in the form of your preference (orally, in writing, electronically, etc.) to Our Telecommunications Details. In the complaint, please describe the circumstances of Your dissatisfaction as accurately as possible and annex the documents that You rely on.</li>
                                                    <li>As a rule, we resolve a complaint within 10 days and inform You about Our position orally, in writing or electronically. If the circumstances of the complaint need further analysis, We have the right to extend the aforementioned time limit.</li>
                                                    <li>If Your complaint is justified, We will resolve the complaint immediately. If We decide to dismiss Your complaint, We will state the reasons to You clearly and comprehensibly.</li>
                                                    <li>Our goal is to settle possible differences by way of negotiations. Failing agreement, You have the right to have recourse to the court. A Court dispute is settled in the court of Our location, unless the Parties agree otherwise or unless otherwise provided by legislation.</li>
                                                </ol>
                                            </ol>
                                            
                                    
                                    </div>
                                </div>
                            </div>


                </div>
               <br> <br> <br> 
            </div>
            <!-- .animated -->
         {{--    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}
        
@endsection