@extends('adminlayout')

@section('content')
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<div class="row" id="profileupdateform">
            <div class="card col-md-12">
                @include('includes.error-logs.custom')
                    <form action="{{ route('submitadminprofileupdate',['id'=>base64_encode($admin->id)]) }}" method="POST">
                        {{ csrf_field() }}
                    <div class="card-header bg-info text-white">Personal Information</div>  
                    <div class="card-body row">
              
                        <div class="col-md-6">
                                <div class="form-group row">
                                        <label for="fname" class="col-md-4 form-label text-md-right">First Name</label>
                                        <div class="col-md-8">
                                            <input type="text" name="fname" class="form-control" value="{{ $admin->fname }}">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="name" class="col-md-4 form-label text-md-right">Username</label>
                                        <div class="col-md-8">
                                            <input type="text" name="username" class="form-control" value="{{ $admin->name }}">
                                        </div>
                                    </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group row">
                                        <label for="phone" class="col-md-4 form-label text-md-right">Phone Number</label>
                                        <div class="col-md-8">
                                            <input type="text" name="phone" class="form-control" value="{{ $admin->phonenumber }}">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email" class="col-md-4 form-label text-md-right">Email Address</label>
                                        <div class="col-md-8">
                                            <input type="email" name="email" class="form-control" value="{{ $admin->email }}">
                                        </div>
                                </div>
                        </div>
                        {{-- <div class="row"> --}}
                                <div class="col-md-12">
                                        <div class="form-group row">
                                                <label for="passwordchange" class="col-md-4 form-label text-md-right">Do you want to change  your password?</label>
                                                <div class="col-md-8">
                                                    <input type="checkbox" id="passwordchange" value="Change Password"> Yes

                                                </div>
                                        </div>
                                </div>
                            {{-- </div> --}}
                      
                </div> 
           <div class="card-footer">
                <form action="{{ route('submitadminprofileupdate',['page'=>str_random(25),'id'=>base64_encode($admin->id)]) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-info btn-sm">
                </form>
           </div>
                     
                    </form>             
    </div>  
</div>
<div class="row" style="display:none" id="passwordchangeform">
    <div class="col-md-12">
        <form action="{{ route('resetpasswordonlogin', $admin->id) }}" method="post">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="oldpassword" class="col-md-4 form-label text-md-right">Old Password</label>
                <div class="col-md-8">
                    <input type="password" name="oldpassword" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="newpassword" class="col-md-4 form-label text-md-right">New Password</label>
                <div class="col-md-8">
                    <input type="password" name="newpassword" class="form-control">
                </div>
            </div>
            <div class="form-group row">
                <label for="confirmpassword" class="col-md-4 form-label text-md-right">Confirm New Password</label>
                <div class="col-md-8">
                    <input type="password" name="confirm_password" class="form-control">
                </div>
            </div>
            <div class="col-md-8 col-md-offset-8">
                <form action="{{ route('resetpasswordonlogin', ['id'=>base64_encode($admin->id)]) }}" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" value="Change" class="btn btn-info btn-sm">
                </form>
            </div>
        </form>
</div>
    </div>
<script>
        $(function () {
            $("#passwordchange").click(function () {
                if ($(this).is(":checked")) {
                    $("#passwordchangeform").show();
                    //$('#profileupdateform').hide();
                    


                } else {
                    $("#passwordchangeform").hide();
                    $('#profileupdateform').show();

                }
            });
            });
</script>
@endsection