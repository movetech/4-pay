@extends('vendorlayout')
@extends('report.base')


@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(count($orders) > 0)
                <table class="table table-responsive table-borderd table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Cart Order</th>
                            <th>Model Number</th>
                            <th>Product Name</th>
                            <th>Size</th>
                            <th>Color</th>
                            <th>Pieces</th>
                            <th>Cost Per Piece</th>
                            <th>Total Cost</th>
                            <th>Status</th>
                            <th>Business Name</th>
                        </tr>
                    </thead>
                    @foreach($orders as $key=>$value)
                        <tbody>
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->customername }}</td>
                                <td>{{ $value->date }}</td>
                                <td>{{ $value->cartorder }}</td>
                                <td>{{ $value->modelnumber }}</td>
                                <td>{{ $value->productname }}</td>
                                <td>{{ $value->size }}</td>
                                <td>{{ $value->color }}</td>
                                <td>{{ $value->pieces }}</td>
                                <td>{{ $value->costperpiece }}</td>
                                <td>{{ $value->totalcost }}</td>
                                <td>{{ $value->status }}</td>
                                <td>{{ $value->bussinessname}}</td>
                            </tr>
                        </tbody>

                        @endforeach
                </table>
                {{ $orders->links() }}
            @endif
        </div>
    </div>
@endsection