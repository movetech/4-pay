@extends('adminlayout')

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @include('includes.error-logs.custom')
            <div class="card">
                <div class="card-header text-white bg-info text-uppercase">{{ $location->name }} <small class="text-lowercase" style="color:#000">(Edit to update)</small></div>
                <div class="card-body">
                    <form action="{{ route('submiteditlocation', ['id'=>$location->id]) }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="name" value="{{ $location->name }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 form-label text-md-right">Description</label>
                            <div class="col-md-8">
                                <textarea name="description" id="" cols="30" rows="10" class="form-control">
                                {{$location->description}}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-send"></i> Update
                            </button>
                            <a href="{{ route('locations') }}" class="btn btn-danger btn-sm pull-right"><i class="fa fa-arrow-left"></i> Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection