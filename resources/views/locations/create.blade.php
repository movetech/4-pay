@extends('adminlayout')

@section('content')
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            @include('includes.error-logs.custom')
            <div class="card">
                <div class="card-header bg-info text-white text-uppercase">{{ __('Add new location') }}</div>
                <div class="card-body">
                    <form action="{{ route('submitlocations') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label for="name" id="name" class="col-md-4 form-label text-md-right">Name</label>
                            <div class="col-md-8">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" name="description" class="col-md-4 form-label text-md-right">Description</label>
                            <div class="col-md-8">
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control" value="{{ old('description') }}">
                                    {{ old('description') }}
                                </textarea>
                            </div>
                        </div>
                        <div class="col-md-8 col-md-offset-4 mb-0">
                            <button type="submit" class="btn btn-info btn-sm">
                                <i class="fa fa-send"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection
<!--assetportal.bigsoft.co.ke-->