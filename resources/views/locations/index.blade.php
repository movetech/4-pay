@extends('adminlayout')

@section('content')
    <div class="row">
        <div class="col-md-12">
                @include('includes.error-logs.custom')
            <div class="card">
                <div class="card-header bg-info text-white text-uppercase">{{ __('Available locations') }}
                <a href="{{ route('locationcreation') }}" class="btn btn-danger btn-sm pull-right">Add New</a>
                </div>
                <div class="card-body">
                    @if(count($locations) > 0)
                        <table class="table table-bordered" style="width:100%">
                            {{ $locations->links() }}
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                                @foreach($locations as $key => $value )
                                    <tbody>
                                        <tr>
                                            <td>{{ $value->id }}</td>
                                            <td>{{ $value->name }}</td>
                                            <td>{{ $value->description }}</td>
                                            <td class="btn-group btn-group-sm">
                                                <a href="{{ route('LocationDetail',['id'=>$value->id]) }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</a>
                                                <a href="{{ route('deletelocation', ['id'=>$value->id]) }}" onclick="if (!confirm('Do you want to delete this record?')){ return false}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endforeach
                        </table>
                        @else
                    <table class="table table-borderd">
                        <td>There are no locations existing, click <a href="{{ route('locationcreation') }}"><span class="text-danger">here</span></a> to add a new location</td>
                    </table>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
@endsection