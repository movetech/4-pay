@extends('viewproductslayout')

@section('content')

<div class="row" id="profileupdateform">
    <div class="col-md-12 text-center  bg-dark" style="margin-bottom:10px"><b>Profile Information(Edit to Update)</b>
        @include('includes.error-logs.custom')
    </div>
    <div class="card">
            <form action="{{ route('submit-update-customer-profile', $user->id) }}" method="post">
                    {{ csrf_field() }}
        <div class="card-header bg-dark text-white"><b></b></div>
        <div class="card-body">
        
    <div class="col-md-6">
            <div class="form-group row {{ $errors->has('vendorname') ? 'has-error':''}}">
                <label for="vendorname" class="col-md-4 form-label text-md-right">Vendor Name</label>
                <div class="col-md-8">
                    <input type="text" name="vendorname" class="form-control" value="{{ $user->vendorname }}">
                   {{--  @if(Session::has('error'))
                    <span class="alert alert-danger">{{ $errors->first('vendorname')}}</span>
                    @endif --}}

                </div>
            </div>
            <div class="form-group row{{ $errors->has('fname')? 'has-error':''}}">
                <label for="fname" class="col-md-4 form-label col-md-4">First Name</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" value="{{ $user->fname }}" name="fname">
                   {{--  @if(Session::has('error'))
                    <span class="alert alert-danger">{{ $errors->first('fname')}}</span>
                    @endif --}}
                </div>
            </div>
            <div class="form-group row{{ $errors->has('name')?'has-error':''}}">
                <label for="name" class="col-md-4 form-label text-md-right">Name</label>
                <div class="col-md-8">
                    <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                   {{--  @if(Session::has('error'))
                    <span class="alert alert-danger">{{ $errors->first('name') }}</span>
                    @endif
 --}}
                </div>
            </div>
            <div class="form-group row{{ $errors->has('location')? 'has-error':''}}">
                <label for="location" class="col-md-4 form-label text-md-right">Location</label>
                <div class="col-md-8">
                    <input type="text" name="location" class="form-control" value="{{ $user->location }}">
                    {{-- @if(Session::has('error'))
                    <span class="alert alert-danger">{{ $errors->first('location') }}</span>
                    @endif --}}
                </div>
            </div>
        
    </div>
    <div class="col-md-6">
        <div class="form-group row{{ $errors->has('phone')? 'has-error':'' }}">
            <label for="phone" class="col-md-4 form-label text-md-right">Phone Number</label>
            <div class="col-md-8">
                <input type="text" name="phone" class="form-control" value="{{ $user->phonenumber }}">
               {{--  @if(Session::has('error'))
                <span class="alert alert-danger">{{ $errors->first('phone')}}</span>
                @endif --}}

            </div>
        </div>
        <div class="form-group row{{ $errors->has('email')? 'has-error':'' }}">
            <label for="email" class="col-md-4 form-label text-md-right">Email</label>
            <div class="col-md-8">
                <input type="email" name="email" value="{{ $user->email }}" class="form-control">
               {{--  @if(Session::has('error'))
                <span class="alert alert-danger">{{ $errors->first('email') }}</span>
                @endif --}}

            </div>
        </div>
        <div class="form-group row{{ $errors->has('idnumber')?'has-error':'' }}">
            <label for="idnumber" class="col-md-4 form-label text-md-right">ID No:</label>
            <div class="col-md-8">
                <input type="text" name="idnumber" class="form-control" value="{{ $user->idno }}">
               {{--  @if(Session::has('error'))
                <span class="alert alert-danger">{{ $errors->first('idnumber') }}</span>
                @endif
 --}}
            </div>
        </div>
        <div class="form-group row{{ $errors->has('address')? 'has-error':'' }}">
            <label for="address" class="col-md-4 form-label text-md-right">Address</label>
            <div class="col-md-8">
                <input type="text" name="address" class="form-control" value="{{ $user->address }}">
               {{--  @if(Session::has('error'))
                <span class="alert alert-danger">{{ $errors->first('address') }}</span>
                @endif --}}

            </div>
           
        </div>
    </div>
    
    </div>
    <div class="card-footer">
            <form action="{{ route('submit-update-customer-profile', $user->id) }}" method="post">
                    <input type="hidden" name="_method" value="PUT"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-primary btn-sm" value="Update">
            </form>
    </div>
</form>
</div>

</div>
<div class="row">
        <div class="col-md-12">
                <div class="form-group row">
                        <label for="passwordchange" class="col-md-4 form-label text-md-right">Do you want to change  your password?</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="passwordchange" value="Change Password">
                        </div>
                </div>
        </div>
    </div>
<div class="row" style="display:none" id="passwordchangeform">
    <form action="{{ route('customer-password-change', ['id'=>base64_encode($user->id)]) }}" method="post">
        {{ csrf_field() }}
        <div class="form-group row">
            <label for="oldpassword" class="col-md-4 form-label text-md-right">Old Password</label>
            <div class="col-md-8">
                <input type="password" name="oldpassword" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="newpassword" class="col-md-4 form-label text-md-right">New Password</label>
            <div class="col-md-8">
                <input type="password" name="newpassword" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="confirmpassword" class="col-md-4 form-label text-md-right">Confirm New Password</label>
            <div class="col-md-8">
                <input type="password" name="confirmpassword" class="form-control">
            </div>
        </div>
        <div class="col-md-8 col-md-offset-4">
            <form action="" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="submit" value="Change" class="btn btn-primary btn-sm">
            </form>
        </div>
    </form>
</div>
<script>

        $(function () {
            $("#passwordchange").click(function () {
                if ($(this).is(":checked")) {
                    $("#passwordchangeform").show();
                    $('#profileupdateform').hide();
                    


                } else {
                    $("#passwordchangeform").hide();
                    $('#profileupdateform').show();

                }
            });
            });
</script>
@endsection