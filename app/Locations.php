<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    //declare the table associated with this model
    protected $table = 'locations';
    //add mass asgnibale fields
    protected $fillable = ['name','description'];
}
