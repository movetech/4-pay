<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Locations;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class LocationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
        $locations = Locations::latest()->paginate(10);
        return view('locations.index', compact('locations'));
    }
    public function createLocation()
    {
        return view('locations.create');
    }
    public function submitLocation(Request $request)
    {
        $v = Validator::make($request->all(), array(
                'name'=>'required',
                'description'=>'required'
        ));
        if($v->fails())
        {
            //$request->session()->flash('alert-danger', $v->errors());
            return redirect()->back()->withInput()->with('error',$v->errors());
        }
        else
        {
            $location = new Locations;
            $location->name = $request->name;            
            $location->description = $request->description;
            if($location->save())
            {
                //$request->session()->flash('alert-success','Location added successfully');
                return redirect()->back()->with('success','Location added successfully');
            }
            else
            {
                //$request->session()->flash('alert-danger','Could not add location, try again');
                return redirect()->back()->withInput()->with('error','Could not add location, try again');
            }

        }
    }
    public function show(Request $request)
    {
        $id = $request->id;
        if(!$id)
        {
            return redirect()->back()->with('error','Location not found');
        }
        else
        {
            $location = Locations::where('id', $id)->first();
            if($location)
            {
                return view('locations.show', compact('location'));
            }
            else
            {
                return redirect()->to(route('locations'))->with('error','Requested location not found');
            }
        }
    }
    public function updateLocationDetails(Request $request)
    {
        $id = $request->id;
        if(!$id)
        {
            return redirect()->back()->withInput()->with('error','Location unavailable');
        }
        else
        {
            $result = Locations::where('id', $id)->first();
            if($result)
            {
                $v = Validator::make($request->all(), array(
                    'name'=>'required',
                    'description'=>'required'
                ));
                if($v->fails())
                {
                    return redirect()->back()->withInput()->with('error',$v->errors());
                }
                else
                {
                    if(Locations::where('id',$id)->first()->update(['name'=>$request->name,'description'=>$request->description]))
                    {
                        return redirect()->back()->with('success','Location updated successfully');
                    }
                    else
                    {
                        return redirect()->back()->withInput()->with('error','Failed to update location details');
                    }
                }
            }
            else
            {
                return redirect()->back()->with('error','The requested location not found');
            }
        }
    }
    public function remove(Request $request)
    {
        $id = $request->id;
        if(!$id)
        {
            return redirect()->back()->with('error','Location not found');
        }
        else
        {
            $v = Validator::make($request->all(),['id'=>'required']);
            if($v->fails())
            {
                return redirect()->back()->with('error', $v->errors());
            }
            else
            {
                $result = Locations::where('id', $id)->first();
                if($result){
                    if(Locations::where('id',$id)->first()->delete())
                    {
                        return redirect()->back()->with('success','Location information deleted successfully');
                    }
                    else
                    {
                        return redirect()->back()->with('error','Could not delete the requested data');
                    }
                }
            }
        }
    }
}
