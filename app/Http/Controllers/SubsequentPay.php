<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CartOrder;
use App\Reminders;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\SMSSetting;
use App\STKpush;

class SubsequentPay extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function initiatePay()
    {
        $userphone=Auth::user()->phonenumber;
        $id=Auth::user()->id;
    $check_next_payment=Reminders::where('user_id',$id)->where('status','pending')->first();
    if($check_next_payment){
        date_default_timezone_set('Africa/Nairobi');
            $BusinessShortCode ='400153';
            $PartyB=$BusinessShortCode;
            $LipaNaMpesaPasskey ='53c4b78a6a03180c4bc923650161b2eea4ceefdd550e91d5341463cc83abbe63';     
            $CallBackURL="https://b2ef0f32.ngrok.io/api/subsequent";
            $PartyA ="254".(int)$userphone; // This is your phone number, 
            $PhoneNumber ="254".(int)$userphone;
            $AccountReference =$check_next_payment->CartNo.":second pay";
            $TransactionDesc ='CART PAYMENT';
            $Amount =$check_next_payment->amount;
            $Timestamp =date('YmdHis');    
            $Remarks="Thank You for Shopping with us";
            $TransactionType="CustomerPayBillOnline";
            # header for access token
            $mpesa= new \Safaricom\Mpesa\Mpesa();
            $stkPushSimulation=$mpesa->STKPushSimulation($BusinessShortCode, $LipaNaMpesaPasskey, $TransactionType, 
            $Amount, $PartyA, $PartyB, $PhoneNumber, $CallBackURL, $AccountReference, $TransactionDesc, $Remarks);
            $json = json_decode($stkPushSimulation,TRUE);
            $push=new STKpush;
             $push->MerchantRequestID=$json['MerchantRequestID'];
             $push->CheckoutRequestID=$json['CheckoutRequestID'];
             $push->ResponseCode=$json['ResponseCode'];
             $push->user=Auth::user()->id;
             $push->phone=$PhoneNumber;
             $push->Ref=$check_next_payment->CartNo;
             $push->save();
            //print_r($stkPushSimulation);
            // return $stkPushSimulation;
            return redirect()->back();
    }
    }
  
}

