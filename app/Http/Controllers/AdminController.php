<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use PHPMailer;
use App\Admin;
use Auth;
use Hash;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }
    public function profile(Request $request)
    {
        $id = Auth::user()->id;
        $admin = Admin::where('id', $id)->first();
        return view('admin.profile.index', compact('admin'));
    }
    public function updateprofile(Request $request, $id)
    {
        $v = Validator::make($request->all(), [
                'fname'=>'required',
                'username'=>'required',
                'phone'=>'required',
                'email'=>'required'
        ]);
        if($v->fails())
        {
            //$request->session()->flash('error',$v->errors());
            return redirect()->back()->withInput()->with('error',$v->errors());
        }
        else
        {
            $id                 =         Auth::user()->id;
            $admin              =         Admin::where('id',$id)->first();
            $admin->fname       =         $request->fname;
            $admin->name        =         $request->username;
            $admin->email       =         $request->email;
            $admin->phonenumber =         $request->phone;
            if($admin->save())
            {
                //$request->session()->flash('success','Profile updated successfully');
                return redirect()->back()->with('success','Profile updated successfully');
            }
            else
            {
                //$request->session()->flash('error','Failed to update profile, try again');
                return redirect()->back()->withInput()->with('error','Failed to update profile, try again');
            }
        }
    }
    public function changePasswordOnLogin(Request $request, $id)
    {
        $admin       = Admin::find($id);
        //set the validation rules
        $validator = Validator::make($request->all(), array(
            'oldpassword'=>'required',
            'newpassword'=>'required',
            'confirm_password'=>'required' 
        ));
        if($validator->fails())
        {
            //if the above validation rules are not met
            //$request->session()->flash('alert-danger',$validator->errors());
            return redirect()->back()->with('error',$validator->errors());
        }
        else
        {
            //if the validation rules have been met, proceed to get user data
            //$id          = Auth::user()->id;
            $pwd         = Auth::user()->password;
            $oldpassword = $request->oldpassword;
            $newpassword = $request->newpassword;
            $confirmpassword = $request->confirm_password;
            if(!Hash::check($oldpassword, $pwd))
                {
                    //if the entered old password does not match the user's previous password
                    //$request->session()->flash('alert-danger','Please check your old password before you proceed');
                    return redirect()->back()->with('error','Please check your old password before you proceed');
                }
            if($confirmpassword !== $newpassword)
                {
                    return redirect()->back()->with('error','Password confirmation failed, try again');

                } 
            /* else if(!Hash::check($newpassword,$confirmpassword))
            {
                //if the newpassword does not match the confirm password
                //$request->session()->flash('alert-danger','Password confirmation failed, try again');
                return redirect()->back()->with('error','Password confirmation failed, try again');
            } */
            /* else
            { */
                $admin = Admin::findOrFail($id);
                $admin->password = bcrypt($request->newpassword);
                if(Admin::where('id',Auth::user()->id)->update(['password'=>bcrypt($newpassword)]))
                {
                    //$request->session()->flash('alert-success','Password updated successfully');
                    return redirect()->back()->with('success','Password updated successfully');
                }
                else
                {
                    //$request->session()->flash('alert-danger','Failed to update password, try again');
                    return redirect()->back()->with('error','Failed to update password, try again');
                }
            // }
        }
    }
}

