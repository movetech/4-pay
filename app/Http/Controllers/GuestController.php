<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Vendor;
use DB;
use Illuminate\Support\Facades\Cache;
use Session;
use Redirect;
use App\User;
use Auth;
use Illuminate\Support\Facades\Validator;
class GuestController extends Controller
{
    public function index()
    {
       
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $categories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC limit 5 ") );
       // $subcategories=DB::select( DB::raw( "SELECT * FROM subcategories ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
       
        return view('guestmainlayout')->with(compact('vendors','categories','leftcategories'));
    }

    public function trendingproduct(Request $request){
        $productname=urldecode($request->productname);
        $id=$request->id;
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        // $products=Product::where('id', $id)->first();
        // if($products){
        //     $category=$products->category;
        //     $subcategory=$products->subcategory;
            
        //     $results=Product::where('productname', 'LIKE', '%' . $productname . '%')->get();
        //     return view('viewproductbody')->with(compact('results','vendors','leftcategories'));

        // }else{
           
            $results=Product::where('productname', 'LIKE', '%' . $productname . '%')->get();
            return view('viewproductbody')->with(compact('results','vendors','leftcategories'));
        // }
        
    }
    
    public function singleitem(Request $request){
        $productname=urldecode($request->productname);
        $id=$request->id;
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        $products=Product::where('id', $id)->first();
        if($products){
            
            $results=Product::where('productname', 'LIKE', '%' . $productname . '%')->get();
            return view('viewsingleproductbody')->with(compact('results','vendors','leftcategories','products'));

        }else{
            
            $results=Product::where('productname', 'LIKE', '%' . $productname . '%')->get();
            return view('viewproductbody')->with(compact('results','vendors','leftcategories'));
        }
        
    }

    public function subcategories(Request $request){
        $category=urldecode($request->category);
        $subcategory=urldecode($request->subcategory);
        $results=Product::where('category',  $category)->where('subcategory',  $subcategory)->get();
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        return view('viewproductbody')->with(compact('results','vendors','leftcategories'));
        
    }

    public function bra_nds(Request $request){

        $bussinessaliasname=urldecode($request->bussinessaliasname);
        $results=Product::where('bussinessname',  $bussinessaliasname)->orderBy('id','desc')->get();
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        return view('viewproductbody')->with(compact('results','vendors','leftcategories'));
        
    }

    public function cate_gory(Request $request)
    {
        $category=urldecode($request->category);
        $results=Product::where('category',  $category)->get();
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        return view('viewproductbody')->with(compact('results','vendors','leftcategories'));
        
    }
    public function profile(Request $request)
    {
        $user = Auth::user();
        $vendors=DB::select( DB::raw( "SELECT * FROM vendors ORDER BY priority  + 0 DESC" ) );
        $leftcategories=DB::select( DB::raw("SELECT * FROM categories ORDER BY priority  + 0 DESC") );
        return view('customer.profile', compact(['vendors','leftcategories','user']));
    }
    public function submitProfileUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(),array(
            'vendorname'=>'required',
            'fname'=>'required',
            'name'=>'required',
            'location'=>'required',
            'phone'=>'required',
            'email'=>'required',
            'idnumber'=>'required',
            'address'=>'required'
        ));
        if($validator->fails())
        {
            $request->session()->flash('error',$validator->errors());
            return redirect()->back()->withInput();
        }
        else
        {
            $user                   =       User::find($id);
            $user->vendorname       =       $request->vendorname;
            $user->fname            =       $request->fname;
            $user->name             =       $request->name;
            $user->location         =       $request->location;
            $user->phonenumber      =       (int)$request->phone;
            $user->email            =       $request->email;
            $user->idno             =       $request->idnumber;
            $user->address          =       $request->address;            
            if($user->save())
            {
                //$request->session()->flash('success','Profile updated successfully');
                return redirect()->back()->with('success','Profile updated successfully');
            }
            else
            {
                //$request->session()->flash('error','Failed to update profile,try again');
                return redirect()->back()->withInput()->with('error','Failed to update profile, try again');
            }
           
        }
        

    }
    public function changePassword(Request $request,$id)
    {
        $user = Auth::user();
        $v = Validator::make($request->all(), [
            'oldpassword'=>'required',
            'newpassword'=>'required',
            'confirmpassword'=>'required'
        ]);
        if($v->fails())
        {
            return redirect()->back()->with('error',$v->errors());
        }
        else
        {
            $pwd = Auth::user()->password;
            $oldpassword = md5($request->oldpassword);
            $newpassword = md5($request->newpassword);
            $confirmpassword = md5($request->confirmpassword);
            if($oldpassword !== $pwd)
            {
                return redirect()->back()->with('error','Please check your old password and try again');
            }
            elseif($confirmpassword !== $newpassword)
            {
                return redirect()->back()->withInput()->with('error','Password confirmation failed, try again');
            }
            else
            {
                $user->password = $newpassword;
                if($user->save())
                {
                    return redirect()->back()->with('success','Password updated successfully');
                }
                else
                {
                    return redirect()->back()->with('error','Failed to change password, try again');
                }

            }
        }
    }
    
}
