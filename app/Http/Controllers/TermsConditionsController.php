<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TermsConditionsController extends Controller
{
    //
    public function termsAndConditions()
    {
        return view('terms.terms-and-conditions');
    }
}
